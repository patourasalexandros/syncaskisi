<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\User;
use Psr\Log\LoggerInterface;



class UserController extends AbstractController
{


    // i used doctrine, logger, and only the documentation of symphony
    private $logger;

    public function __construct(LoggerInterface $logger) {
        $this->logger = $logger;
    }
    /**
     * @Route("/user/{id}", name="user")
     */
    public function index(Request $request,$id = "")
    {
        $this->logger->info($request);

        $user = $this->getDoctrine()
        ->getRepository(User::class)
        ->findOneBy(['id' => $id]);
// return $this->json([
//             'id'=>$user->getId(),
//             'name' => $user->getName(),
//             'other_details' => $user->getOtherDetails(),
//             'api_key' => $user->getApiKey()
//         ]);
        if (!$user) {
            throw $this->createNotFoundException(
                'No user found'
            );
        }

        $headers = $request->headers->all();

        if (!array_key_exists('x-api-key', $headers)) {
            return $this->json([
            'result' => 'No key provided'
        ]); 
        }
        
        if($user->getApiKey() != $request->headers->get('x-api-key')) {
            return $this->json([
            'result' => 'get your own user' // i disagree of letting the visitor know if the id is  of an existing user. i believe using uuid is a bettet tactic. but it does not matter for now
        ]); 
        }      


        return $this->json([
            'name' => $user->getName(),
            'other_details' => $user->getOtherDetails()
        ]);
    }

    /**
     * @Route("/users/seed")
     */
    public function seedUsers() {
        // because my time is already limited, knowing that i break the endpoint practices, instead of searching about seeders from symphony i make two objects here. just run the route to create the users

        $entityManager = $this->getDoctrine()->getManager();

        $user1 = new User();
        $user1->setName("John");
        $user1->setApiKey("firstKey"); //although api keys are not like that, i choose it for demonstration reasons
        $entityManager->persist($user1);


        $user2 = new User();
        $user2->setName("George");
        $user2->setApiKey("secondKey");
        $user2->setOtherDetails(["dog"=>false,"car"=>"skarabaios"]);
        $entityManager->persist($user2);

        $entityManager->flush();

        return $this->json([
            'result' => 'table is seeded'
        ]);

    }
}
